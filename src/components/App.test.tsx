import React from 'react';
import ReactDOM from 'react-dom';
import renderer from 'react-test-renderer';
import App from './App';
import SearchBox from './SearchBox';

import * as api from '../api';


it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});

const mockGetDocsAPICall = (result: Promise<any>) => {
  // NOTE: TypeScript doesn't like that we're monkeypatching a const.
  api.getDocs = () => result;
};

describe("document fetching", () => {
  it('renders a loading indicator while fetching documents', done => {
    // Mock API call with promise that never finishes.
    mockGetDocsAPICall(new Promise(() => null));
  
    const app = renderer.create(<App />);
    setTimeout(() => {
      const tree = app.toJSON();
      expect(tree).toMatchSnapshot();
      done();
    }, 0);
  });
  
  it('renders an error if it fails to fetch documents', done => {
    // Mock API call with promise that immediately fails.
    mockGetDocsAPICall(Promise.reject());
  
    const app = renderer.create(<App />);
    setTimeout(() => {
      const tree = app.toJSON();
      expect(tree).toMatchSnapshot();
      done();
    }, 0);
  });
  
  it('renders docs when fetched successfully', done => {
    // Mock API call with promise that immediately succeeds.
    mockGetDocsAPICall(Promise.resolve({
      numTotalDocs: 6,
      numTotalBytes: 600 * 1000,
      docs: [1,2,3,4,5,6].map((i) => { 
        return { id: i, name: `Doc ${i}`, numBytes: 100 * 1000 };
      })
    }));
  
    const app = renderer.create(<App />);
    setTimeout(() => {
      const tree = app.toJSON();
      expect(tree).toMatchSnapshot();
      done();
    }, 0);
  });
});

describe("deletion", () => {
  it("calls the delete API when the delete button is clicked", done => {
    mockGetDocsAPICall(Promise.resolve({
      numTotalDocs: 1,
      numTotalBytes: 100,
      docs: [{ id: 1, name: `Doc 1`, numBytes: 100 }],
    }));
  
    const deleteAPICall = jest.fn();
    api.deleteDoc = deleteAPICall;

    const app = renderer.create(<App />);
    setTimeout(() => {
      const tree = app.toJSON();
      const deleteButton = app.root.findAllByType("button")[1];
      deleteButton.props.onClick();

      setTimeout(() => {
        expect(deleteAPICall).toBeCalled();
        done();
      })
    });
  });
});

describe("searching", () => {
  it("calls the search API when a query is triggered from the search box", done => {
    const app = renderer.create(<App />);

    const searchAPICall = jest.fn();
    api.getDocs = searchAPICall;

    setTimeout(() => {
      const tree = app.toJSON();
      const searchBox = app.root.findByType(SearchBox);
      searchBox.props.onSearch("q");

      setTimeout(() => {
        expect(searchAPICall).toBeCalled();
        done();
      })
    });
  });
});
