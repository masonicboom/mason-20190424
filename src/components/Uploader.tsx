// Uploader manages file uploads.

import React, {Component, RefObject, ChangeEvent} from "react";
import {uploadDoc} from "../api";

interface Props {
  onUploadSuccess: () => void;
}

export default class Uploader extends Component<Props> {
  fileInputRef: RefObject<HTMLInputElement> = React.createRef();

  handleUploadClick = () => {
    const ref = this.fileInputRef;
    ref && ref.current && ref.current.click();
  }

  handleFileSelected = async (event: ChangeEvent<HTMLInputElement>) => {
    const el = this.fileInputRef && this.fileInputRef.current;
    if (!el || !el.files || !el.files.length) {
      return;
    }
    const file = el.files[0];
    
    try {
      await uploadDoc(file);
      this.props.onUploadSuccess();
    } catch (e) {
      alert(`Failed to upload document: ${e}`);
    }
  }

  render() {
    return (
      <div id="upload">
        <label>
          <button onClick={this.handleUploadClick} style={{width: "100%"}}>
            Upload
          </button>
          <input
            type="file"
            ref={this.fileInputRef}
            style={{display: "none"}}
            accept="image/png, image/jpeg"
            onChange={this.handleFileSelected}
          />
        </label>
      </div>
    );
  }
}
