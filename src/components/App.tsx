// App manages the full application state and UI hierarchy.

import React from 'react';
import ResultControls from "./ResultControls";
import DocumentList from "./DocumentList";
import SearchBox from "./SearchBox";
import Uploader from "./Uploader";
import {getDocs, GetDocsResponse} from "../api";
import {Document} from "../types";

interface State {
  loading: boolean;
  loadFailed: boolean;
  getDocsResponse?: GetDocsResponse;
  query?: string;
}
export default class App extends React.Component<any, State> {
  constructor(props: any) {
    super(props);
    
    this.state = {
      loading: true,
      loadFailed: false,
    };
  
    this.fetchDocs();
  }

  fetchDocs = async (query?: string) => {
    try {
      const resp = await getDocs(query);
      this.setState({
        loading: false,
        loadFailed: false,
        getDocsResponse: resp,
      });
    } catch (e) {
      this.setState({
        loading: false,
        loadFailed: true,
      });
    }
  }

  handleSearch = (query?: string) => {
    this.setState({
      loading: true,
      loadFailed: false,
    }, () => {
      this.setState({ query }, () => {
        this.fetchDocs(query);
      });
    })
  }

  clearSearch = () => {
    this.handleSearch(undefined);
  }

  handleUploadSuccess = () => {
    // Re-fetch default results view, to show the newly-uploaded file (assuming it hasn't been pushed below the fold by new content uploaded by other users).
    this.clearSearch();
  }

  handleDelete = (doc: Document) => {
    const {getDocsResponse} = this.state;
    if (!getDocsResponse) {
      return;
    }
    const {numTotalBytes, numTotalDocs, docs} = getDocsResponse;

    this.setState({
      getDocsResponse: {
        docs: docs.filter((d) => d.id !== doc.id),
        numTotalBytes: numTotalBytes - doc.numBytes,
        numTotalDocs: numTotalDocs - 1,
      },
    });
  }

  render() {
    const {loading, loadFailed, getDocsResponse, query} = this.state;
    let body;
    if (loading) {
      body = <div>Loading documents...</div>;
    } else if (loadFailed) {
      body = <div>Failed to fetch documents. Refresh to try again.</div>;
    } else if (getDocsResponse) {
      const {numTotalBytes, numTotalDocs, docs} = getDocsResponse;
      
      body = (
        <React.Fragment>
          <ResultControls
            numTotalDocs={numTotalDocs}
            numTotalBytes={numTotalBytes}
            query={query}
            onClearSearch={this.clearSearch}
          />
          <DocumentList docs={docs} onDelete={this.handleDelete} />
        </React.Fragment>
      );
    } else {
      body = <div>This shouldn't happen. Please contact the developer.</div>
    }
    
    return (
      <div className="app">
        <div className="app-controls">
          <SearchBox onSearch={this.handleSearch} />
          <Uploader onUploadSuccess={this.handleUploadSuccess} />
        </div>

        {body}
      </div>
    );
  }
}
 