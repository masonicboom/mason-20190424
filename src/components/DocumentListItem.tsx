// DocumentListItem presents an individual document.

import React from "react";
import {formatByteCount} from "../lib";
import {Document} from "../types";
import {deleteDoc} from "../api";

interface Props {
  doc: Document;
  onDelete: (doc: Document) => void;
}
interface State {
  pendingDelete: boolean;
}
export default class DocumentListItem extends React.Component<Props, State> {
  state = {
    pendingDelete: false,
  }

  handleDelete = async () => {
    const {doc, onDelete} = this.props;

    this.setState({pendingDelete: true}, async () => {
      try {
        await deleteDoc(doc);
        onDelete(doc);
      } catch (e) {
        alert(`Failed to delete document ("${doc.name}"): ${e}`);
        this.setState({pendingDelete: false});
      }
    });
  }

  render() {
    const {name, numBytes} = this.props.doc;
    const {pendingDelete} = this.state;

    let className = "document-list-item";
    if (pendingDelete) {
      className += " pending-delete";
    }

    return (
      <div className={className}>
        <div className="name">{name}</div>
        <div className="controls">
          <div className="size">{formatByteCount(numBytes)}</div>
          <button className="delete" onClick={this.handleDelete} disabled={pendingDelete}>
            DELETE
          </button>
        </div>
      </div>
    );
  }
}
