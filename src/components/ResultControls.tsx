// ResultControls summarizes info about the presented document results.
// It can also clear the current search query, if any.

import React from "react";
import {formatByteCount} from "../lib";

interface Props {
  numTotalDocs: number;
  numTotalBytes: number;
  query?: string;
  onClearSearch: () => void;
}

export default class ResultControls extends React.Component<Props> {
  static documentCountMessage(numDocuments: number, query?: string): string {
    let message = `${numDocuments} document`;
    if (numDocuments !== 1) {
      message += "s";
    }
    if (query) {
      message += ` matching "${query}"`;
    }
    return message;
  }

  render() {
    const {numTotalDocs, numTotalBytes, query, onClearSearch} = this.props;

    const clearSearch = <button onClick={onClearSearch}>clear search</button>;

    return (
      <div className="document-stats">
        <div className="num-results">
          <div className="message">{ResultControls.documentCountMessage(numTotalDocs, query)}</div>
          {query && clearSearch}
        </div>
        <div>Total size: {formatByteCount(numTotalBytes)}</div>
      </div>
    );
  }
}
