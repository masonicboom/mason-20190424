import React from 'react';
import renderer from 'react-test-renderer';
import ResultControls from "./ResultControls";

describe("result count message", () => {
  it("doesn't pluralize a single result", () => {
    expect(ResultControls.documentCountMessage(1).startsWith("1 document")).toBeTruthy();
  })

  it("pluralizes for multiple results", () => {
    expect(ResultControls.documentCountMessage(2).startsWith("2 documents")).toBeTruthy();
  })

  it("pluralizes for 0 results", () => {
    expect(ResultControls.documentCountMessage(0).startsWith("0 documents")).toBeTruthy();
  })

  it("includes the query string if there is one", () => {
    expect(ResultControls.documentCountMessage(2, "needle")).toContain('"needle"');
  });
});

it("renders details about the search query and a clear search button, when a search query is provided", () => {
  const app = renderer.create(
    <ResultControls
      numTotalDocs={3}
      numTotalBytes={1234}
      query="needle"
      onClearSearch={() => null}
    />
  );
  const tree = app.toJSON();
  expect(tree).toMatchSnapshot();
});

it("renders no clear button or query info, when no search query is provided", () => {
  const app = renderer.create(
    <ResultControls
      numTotalDocs={3}
      numTotalBytes={1234}
      onClearSearch={() => null}
    />
  );
  const tree = app.toJSON();
  expect(tree).toMatchSnapshot();
});