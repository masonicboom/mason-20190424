// SearchBox manages search query input.

import React, {Component, ChangeEvent} from 'react';

interface Props {
  onSearch: (query: string) => void;
}

interface State {
  query: string;
}

export default class SearchBox extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      query: "",
    };
  }

  handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    const query = (event && event.target && event.target.value) || "";
    this.setState({query: query});
  }

  handleSearch = () => {
    const {onSearch} = this.props;
    const {query} = this.state;

    if (query === "") {
      return;
    }

    this.setState({query: ""});
    onSearch(query);
  }

  handleKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === "Enter") {
      this.handleSearch();
    }
  }

  render() {
    const {query} = this.state;
    return (
      <input
        id="search"
        type="search"
        placeholder="Search documents..."
        onChange={this.handleChange}
        onKeyDown={this.handleKeyDown}
        value={query}
      />
    )
  }
}
