// DocumentList presents a list of documents.

import React from "react";
import DocumentListItem from "./DocumentListItem";
import {Document} from "../types";

interface Props {
  docs: Document[];
  onDelete: (doc: Document) => void;
}

export default class DocumentList extends React.Component<Props> {
  render() {
    const {docs, onDelete} = this.props;
    const items = docs.map((doc) => {
      return <DocumentListItem key={doc.id} doc={doc} onDelete={onDelete} />
    });

    return (
      <div className="document-list">
        {items}
      </div>
    );
  }
}
