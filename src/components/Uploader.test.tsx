import React from 'react';
import renderer from 'react-test-renderer';
import Uploader from './Uploader';

import * as api from '../api';

describe("uploading", () => {
  it("calls the upload API when a file is uploaded from the uploader", done => {
    const app = renderer.create(
      <Uploader onUploadSuccess={() => null} />,
      {
        createNodeMock: (el) => {
          // This mocks the file input to already have a "file" selected.
          if (el.type === "input") {
            return { files: ['f'] }
          }
        }
      }
    );

    const uploadAPICall = jest.fn();
    api.uploadDoc = uploadAPICall;

    setTimeout(() => {
      const tree = app.toJSON();
      const uploader = app.root.findByType("input");
      uploader.props.onChange();

      setTimeout(() => {
        expect(uploadAPICall).toBeCalled();
        done();
      })
    });
  });
});