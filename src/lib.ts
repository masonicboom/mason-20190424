// lib holds convenience functions.

import numeral from "numeral";

export const formatByteCount = (numBytes: number): string => {
  return numeral(numBytes).format("0b");
};

export const genRandomHexId = (): string => {
  const randVals = new Uint8Array(16);
  return Array.from(randVals).map(x => x.toString(16)).join("")
}