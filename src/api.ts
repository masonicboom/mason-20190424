// api provides the client's interface to the server.

import {Document} from "./types";
import MockServer from "./server";

export interface GetDocsResponse {
  numTotalDocs: number;
  numTotalBytes: number;
  docs: Document[];
}

const server = new MockServer();

export async function getDocs(query?: string): Promise<GetDocsResponse> {
  return server.getDocs(query);
}

export async function uploadDoc(doc: File): Promise<string> {
  return server.uploadDoc(doc);
}

export async function deleteDoc(doc: Document): Promise<any> {
  return server.deleteDoc(doc.id);
}
