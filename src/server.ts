// server provides a mock server implementation.

import {Document} from "./types";
import {genRandomHexId} from "./lib";
import {GetDocsResponse} from "./api";

const maxDocBytes = 10 * 1024 * 1024;

export default class MockServer {
  files: Map<string, File>;
  docs: Document[];

  constructor() {
    this.files = new Map();
    this.docs = [1,2,3,4,5,6].map((i) => { 
      return {
        id: `${i}`,
        name: `Doc ${i}`,
        numBytes: 100 * 1000,
      };
    });
  }

  async getDocs(query?: string): Promise<GetDocsResponse> {
    return new Promise<GetDocsResponse>((resolve) => {
      setTimeout(() => {
        let docs = this.docs;
        if (query) {
          docs = docs.filter((d) => d.name.toLowerCase().includes(query.toLowerCase()))
        }

        let numTotalBytes = 0;
        docs.forEach((d) => numTotalBytes += d.numBytes);

        return resolve({
          docs,
          numTotalBytes,
          numTotalDocs: docs.length,
        });
      }, 500);
    });
  }

  async uploadDoc(doc: File): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      setTimeout(() => {
        if (doc.size > maxDocBytes) {
          reject("Uploaded file is too large.");
        }

        const id = genRandomHexId();
        this.files.set(id, doc);
        this.docs.push({
          id,
          name: doc.name,
          numBytes: doc.size,
        });

        resolve(id);
      }, 500);
    });
  }

  async deleteDoc(id: string): Promise<any> {
    return new Promise<string>((resolve, reject) => {
      setTimeout(() => {
        this.files.delete(id);
        this.docs = this.docs.filter((d) => d.id !== id);

        resolve();
      }, 500);
    });
  }
}
