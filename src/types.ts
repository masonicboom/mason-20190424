// types holds app-wide types.

export interface Document {
  id: string;
  name: string;
  numBytes: number;
}
