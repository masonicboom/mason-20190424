# Mason - 2019-04-24

## Installation

1. Install [yarn](https://yarnpkg.com).
1. `yarn`
1. `yarn start`

## Security

1. Malicious user could attempt to traverse file system and delete files on the server by crafting a filename containing directory traversal commands (e.g. `../../`). Addressed this by deleting files by not interpolating user-provided filename into a delete command, but instead deleting from (mock) data store using server-generated ID.
1. Malicious user could attempt SQL injection in search query. Addressed by using a string search method that doesn't evaluate user input as code. If using a real SQL database rather than my mock, I'd use the DB driver's placeholder substitution rather than raw string interpolation in the host language (e.g. https://golang.org/pkg/database/sql/#DB.Exec).
1. Malicious user could attempt XSS by injecting HTML/JS with a specially-crafted filename for an image they upload. Addressed by using React's default mechanism to include strings in the DOM, rather than dangerouslySetInnerHTML.
1. Malicious user could attempt to overwrite another user's uploaded file by uploading a file with the same name. Addressed by assigning each file a (probabilistically) unique ID on the server, and indexing the data store using that ID, rather than filename.
1. Malicious user could upload imagery to shock or disgust other users. Addressed by not displaying uploaded images.
1. Malicious user could display offensive text to other users by crafting an offensive filename for an image they upload. Did not address. Could address with a word blacklist that blocks upload or eliminates blacklisted word from filename before presentation in UI.
1. Malicious user could run a DoS attack by exhausting the server's bandwidth with a lot of requests. Did not address. If actually deploying, would address by using a cloud provider like AWS or CloudFlare with DoS protection.
1. Malicious user could exhaust storage space or run up a large storage bill by uploading a lot of files. Did not address. Could address by requiring payment or proof-of-work (computational--like bitcoin, or captcha-based).

## Improvements

- Pagination/infinite-scroll
- UI styling
- Search typeahead suggestions
- Sorting of search results
- ... many more things but basically depends on the actual use-case

## Libraries

- [numeral](http://numeraljs.com/), for formatting file sizes.

## API
// Any general observation about the API?

I chose to implement a mock API for simplicity. If it were an actual REST server, I'd use these endpoints.

### GET /docs

Retrieves the list of uploaded documents.

Returns JSON like:

    {
      numTotalDocs: number,
      numTotalBytes: number,
      docs: [
        {
          id: string,
          name: string,
          numBytes: number,
        },
        ...
      ]
    }

Parameters:

- `query` -- optional string specifying search query to filter results

### POST /docs/upload

Accepts document name and generates a URL for client to upload file contents directly to storage (e.g. S3).

Returns JSON like:

    {
      uploadUrl: string
    }

...where `uploadUrl` is a URL the client should now `POST` the image contents to, directly.

Parameters sent as JSON in request body, like:

    {
      name: string,
      authenticityToken: string,
    }

`authenticityToken` is verified by the server to check that client is entitled to upload a file (e.g. https://developers.google.com/recaptcha/docs/verify).

### DELETE /docs/:id

Deletes the document with specified `id`.

Returns nothing. Status code 200 indicates success. Status codes in the error range indicate failure.

Parameters: none.

## Other notes
